#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
 
#define THINKING_TIME 3  
#define N_PHILOSOPHERS 3
#define EATING_PERIOD 1  
#define EATING_TIME 2 
#define MEAL_COUNT 3  

char* philosophers_names[] = { "Phil[1]", "Phil[2]", "Phil[3]", "Phil[4]", "Phil[5]", "Phil[6]", "Phil[7]" };
 
typedef struct PhilosopherInfo {
  char* name;
  int meals;
  int id;
} philosopherInfo;
 
pthread_mutex_t fork_mutex[N_PHILOSOPHERS];
 
void grab_forks( char* name, int left_fork_id ) {
  int right_fork_id = left_fork_id - 1;
  if ( right_fork_id < 0 )
    right_fork_id = N_PHILOSOPHERS - 1;
  
  printf( "%s is trying to grab left fork %d\n", name, left_fork_id );
  if ( pthread_mutex_lock(&fork_mutex[left_fork_id]) != 0 ) printf("pthread_mutex_lock error\n");
  printf( "%s has left fork %d.\n", name, left_fork_id );
  printf( "%s is trying to grab right fork %d\n", name, right_fork_id );
  if ( pthread_mutex_lock(&fork_mutex[right_fork_id]) != 0 ) printf("pthread_mutex_lock error\n");
  printf( "%s has right fork %d.\n", name, right_fork_id );
  printf( "%s has both forks (left: %d and right: %d)\n", name, left_fork_id, right_fork_id );
}
 
void put_away_forks(char* name, int left_fork_id) {
  int right_fork_id = left_fork_id - 1;
  if (right_fork_id < 0) right_fork_id = N_PHILOSOPHERS - 1;
 
  printf( "%s put away the forks (left: %d and right: %d)\n", name, left_fork_id, right_fork_id );
 
  if (pthread_mutex_unlock(&fork_mutex[left_fork_id]) != 0) printf("pthread_mutex_unlock error\n");
  if (pthread_mutex_unlock(&fork_mutex[right_fork_id]) != 0) printf("pthread_mutex_unlock error\n");
}
 
void eat(int id, char* name, int meal_number) {
  grab_forks(name, id);
  printf("%s is using the forks and eating.\n", name);
  sleep(EATING_TIME);
  printf("%s finished meal number %d.\n", name, meal_number);
  put_away_forks(name, id);
}
 
void think(char* name) {
  printf("%s is thinking!\n", name);
  sleep(THINKING_TIME);
}
 
void* philosopher_thread(void* vargs) {
  philosopherInfo* args = (philosopherInfo*) vargs;
  int total_meals = args->meals;
  int hungry = 0;
 
  printf("%s arrives to the table\n", args->name);
 
  while (args->meals) {
    if (hungry) {
      eat(args -> id, args -> name, total_meals - args -> meals);
      args -> meals -= 1;
      hungry = 0;
    } else {
      think( args->name );
      hungry = 1;
    }
  }
  free(args);
 
  return NULL;
}
 
int main() {
  srand(time(NULL));
 

  // PThread Initializations.
  for (int i = 0; i < N_PHILOSOPHERS; ++i) pthread_mutex_init(&fork_mutex[i], NULL);
 
  pthread_t philosophers[N_PHILOSOPHERS];
  for (int i = 0; i < N_PHILOSOPHERS; ++i) {
    philosopherInfo* args = (philosopherInfo*) malloc(sizeof(philosopherInfo));
    args->name = philosophers_names[i];
    args->id = i;
    args->meals = MEAL_COUNT; 
 
    if (pthread_create(&philosophers[i], NULL, philosopher_thread, (void*) args) != 0) {
      printf("pthread_create failed!\n");
      return -1;
    }
  }
 
  for ( int i = 0; i < N_PHILOSOPHERS; ++i ) {
    if (pthread_join(philosophers[i], NULL) != 0 ) {
      printf("pthread_join failed!\n");
      return -1;
    }
  }
 
  for (int i = 0; i < N_PHILOSOPHERS; ++i) {
    if (pthread_mutex_destroy(&fork_mutex[i]) != 0) {
      printf("pthread_mutex_destroy failed!\n");
      return -1;
    }
    printf("philosopher %s has left the table\n", philosophers_names[i]);
  }
 
  return 0;
}